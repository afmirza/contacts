    //
    // ContactsModel.swift
    // ContactsAssignment
    //
    // Created by The App Experts on 06/03/2020.
    // Copyright © 2020 The App Experts. All rights reserved.
    //
    
    import Foundation
    
    struct ContactsItem: Equatable {
        var fullName: String
        var contacNumber: String?
        
        static func ==(lhs: ContactsItem, rhs: ContactsItem) -> Bool{
            return lhs.fullName == rhs.fullName && lhs.contacNumber == rhs.contacNumber
        }
    }
    
    class ContactsModel{
        
        private var contact: [String: [ContactsItem]]
        init() {
            
            contact = [:]
        }
        
        func compareContacts(firstElement: ContactsItem, secondElement: ContactsItem) -> Bool{
            if firstElement.fullName.lowercased() < secondElement.fullName.lowercased() {return true}
            else{ return false}
        }
        
        // Does not protect against duplicate Entries
//        // function for sorting, starting with basics to get better understanding
//        func saveContact(_ name: String,number: String?, key: String ){
//            
//            let tempContactItem: ContactsItem = ContactsItem(fullName: name, contacNumber: number)
//            
//            // adding values to existing keys
//            if var array = contact[key]{
//                // for value in array{
//                // if array.contains(where: {($0.fullName != value.fullName)}){
//                array.append(tempContactItem)
//                let sortedArray = array.sorted(by: compareContacts)
//                contact.updateValue(sortedArray, forKey: key)
//                
//                
//            } else{
//                
//                contact.updateValue([tempContactItem], forKey: key)
//            }
//        }

        
            // This protects against duplicate entries, protection aginst duplicate values
        func saveContact(contactItem: ContactsItem, key: String){
            
                if var array = contact[key]{
                    if array.contains(contactItem){
                        // do nothing
                    }else{
                        array.append(contactItem)
                        let sortedArray = array.sorted(by: compareContacts)
                        contact.updateValue(sortedArray, forKey: key)}
                    
                } else{
                    
                  contact.updateValue([contactItem], forKey: key)
                }

        }
        
        // helper method to inform user that contact already exists to show results via alert,
        func contactAlreadyExists(contactItem: ContactsItem, key: String) -> Bool{
            
            guard let array = contact[key] else{return false}
            if array.contains(contactItem){
                return true
            }else {return false}
        }
        
    }
    
    extension ContactsModel{
        var sections: [String]{
            return contact.keys.sorted()
        }
        
        func numberOfEntries(in section: Int) -> Int {
            
            guard section < sections.count else {return 0}
            
            let key = sections[section]
            guard let values = contact[key] else {return 0}
            
            return values.count
        }
        
        func contactEntry(atIndexPath indexPath: IndexPath) -> ContactsItem?{
            
            guard indexPath.section < sections.count else {return nil}
            
            let key = sections[indexPath.section]
            guard let values = contact[key], indexPath.row < values.count else {return nil}
            
            return values[indexPath.row]
            
        }
    }
