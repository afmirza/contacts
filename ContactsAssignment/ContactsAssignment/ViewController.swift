//
//  ViewController.swift
//  ContactsAssignment
//
//  Created by The App Experts on 06/03/2020.
//  Copyright © 2020 The App Experts. All rights reserved.
//

import UIKit

class CustomCell: UITableViewCell{
    @IBOutlet weak var pictureView: UIImageView!
    @IBOutlet weak var nameLabelView: UILabel!
    @IBOutlet weak var phoneImageView: UIImageView!
    
}

class ViewController: UIViewController {


    @IBOutlet weak var nameTextField: UITextField!
    
    @IBOutlet weak var phoneNumberTextField: UITextField!
    
    @IBOutlet weak var tableView: UITableView!
    var model: ContactsModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    model = ContactsModel()
        
        tableView.dataSource = self
        tableView.delegate = self
        tableView.tableFooterView = UIView(frame: CGRect.zero)
//        self.tableView.register(CustomCell.self, forCellReuseIdentifier: "cellID")
    }
    
    @IBAction func insertContact(_ sender: Any) {
        
        guard let name = nameTextField.text else {return}
        guard let phoneNumber = phoneNumberTextField.text else {
            return
        }
        
        if name.isEmpty{
            
            // If name empty true, display an alert
            let alert = UIAlertController(title: "Alert", message: "Please enter valid name", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            present(alert, animated: true, completion: nil)
        }else{
   
            let contactItem = ContactsItem(fullName: name, contacNumber: phoneNumber)
            // get first character of the string to make key 
        let key = String(name[name.startIndex])
            
            // check if contact already exists and inform user via alert
            
            if model.contactAlreadyExists(contactItem: contactItem, key: key){
                let alert = UIAlertController(title: "Alert", message: "Contact already exists", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                present(alert, animated: true, completion: nil)
            }else {
                
        // save contact in model and update table
                model.saveContact(contactItem: contactItem, key: key.uppercased())

                tableView.reloadData()
                nameTextField.text = nil
                phoneNumberTextField.text = nil
            }

        }
    }
    
    func showActionSheet(item: ContactsItem) {
        
        let alertController = UIAlertController(title: "Call Them, Maybe", message: "Hey, just met you, and here's their number so call them mabe?", preferredStyle: .actionSheet)
        
        guard let contactsPhoneNumber = item.contacNumber else {return }
        
        // if contact has no phone number
        if contactsPhoneNumber.isEmpty {
            
                let alert = UIAlertController(title: "Alert", message: "Contact has no phone number", preferredStyle: .alert)
                   alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                   present(alert, animated: true, completion: nil)
            
        } else {

        
        
        let viaPhoneAction = UIAlertAction(title: "Via Phone", style: .default) { action in

            return
            
        }
        alertController.addAction(viaPhoneAction)
        
        let viaSkypeAction = UIAlertAction(title: "Via Skype", style: .default) { action in

            return
        }
        alertController.addAction(viaSkypeAction)
        
        let viaWhatsAppAction = UIAlertAction(title: "Via Whats App", style: .default) { action in
            return
        }
        alertController.addAction(viaWhatsAppAction)
        
        let destroyAction = UIAlertAction(title: "How About No", style: .destructive) { action in
            return
        }
        alertController.addAction(destroyAction)
        
        
        present(alertController, animated: true, completion: nil)
        
    }
        
    }

}

extension ViewController: UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        guard let sections = model?.sections.count else {return 1}
        return sections
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return model.numberOfEntries(in: section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellID", for: indexPath) as! CustomCell
        
        guard let contactEntry = model.contactEntry(atIndexPath: indexPath) else {return cell}
        
        guard let phoneNumber = contactEntry.contacNumber else{ return cell}
        
        cell.pictureView.image = UIImage(named: "image_placeholder")
        cell.nameLabelView.text = contactEntry.fullName
        
        if phoneNumber.isEmpty{
            cell.phoneImageView.image = UIImage(named: "")
            
        }else{
             cell.phoneImageView.image = UIImage(named: "phone")
        }
        

        return cell
    }
    
       func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return model.sections[section]
    }
    
    
    
}

extension ViewController: UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let selectedItem = model.contactEntry(atIndexPath: indexPath) else { return }
        showActionSheet(item: selectedItem)
    }
}

